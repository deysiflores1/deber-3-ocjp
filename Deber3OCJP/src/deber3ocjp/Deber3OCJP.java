/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deber3ocjp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author USRKAP
 */
public class Deber3OCJP {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        
         System.out.println("Escuela Politecnica Nacional");
         System.out.println("Deber de Oracle Certified Java Programmer");
         System.out.println("Autor:Deysi Tacán");
         System.out.println("Uso de tipo de datos primitivos");
         System.out.println("1. Uso de char: Imprime las letras del alfabeto");
        char[] s;
        s=new char[26];
        for ( int i=0; i<26; i++) {
            s[i] = (char) ('a' + i ); 
        System.out.print((char) ('a' + i )+"   ");
        } 
        System.out.println("");
        System.out.println("2. Uso de byte:Operaciones basicas");    
        byte n1 = 4;
        byte n2 = 3;
         
        byte suma =  (byte) (n1 + n2);
        byte division =  (byte) (n1/n2);
        byte multiplicacion = (byte) (n1*n2);
        byte resta=(byte)(n1-n2);
       
        System.out.println("La Suma es:   " + suma);
        System.out.println("La resta es:   " + resta);
        System.out.println("La Division es:    " + division);
        System.out.println("La Multiplicacion es:   " + multiplicacion);
        
        System.out.println("3.Uso de short");
        short n3 = 2;
        short n4 = 3;
        short res = (short) (n3 + n4);
        System.out.println("La respuesta usando short es:   "+res);
        System.out.println("4. Uso de int");
        Scanner sc = new Scanner(System.in);
        int numero,fibona1,fibona2,i;
        do{
            System.out.print("Introduce numero mayor que 1: ");
            numero = sc.nextInt();
        }
        while(numero<=1);
        System.out.println("Los " + numero + " primeros numeros de la serie de Fibonacci son:"); 

        fibona1=1;
        fibona2=1; 

        System.out.print(fibona1 + " ");
        for(i=2;i<=numero;i++){
             System.out.print(fibona2 + " ");
             fibona2 = fibona1 + fibona2;
             fibona1 = fibona2 - fibona1;
        }
        System.out.println();
        
        System.out.println("5. Uso de long");
        long masa;
        masa = 9223372036854775807l;
        System.out.println("Long: "+masa);
        
        System.out.println("6. Uso de float");
        float dinero;
        BufferedReader br;                      
               
        br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Ingrese su monto actual $");
        dinero = Float.parseFloat(br.readLine());
        System.out.println("Usted tiene   "+dinero+"    dolares");
        
        System.out.println("Convertir entero a double");

        int j = 7; 
        double r; 
        r = j*1.0; 
        System.out.println(""+r);
        System.out.println("Uso de boolean");
            boolean x = true;
          boolean y = false;
          boolean a1 = x && x;
          boolean a2 = x && y;
          boolean a3 = y && x;
          boolean a4 = y && y;

          System.out.println("Tabla de verdad de la conjunción( Y)");
          System.out.println( x + " &&" + x + "  = " + a1 );
          System.out.println( x + " &&" + y + "  = " + a2 );
          System.out.println( y + " &&" + x + "  = " + a3 );
          System.out.println( y + " &&" + y + "  = " + a4 );
          
          System.out.println("Uso de operadores ");
          System.out.println("Operadores unarios");
          int z,w,t;
          z = 5;
          w = z ++;
          System.out.println("El valor de z es   "+z+"   El valor de w es:  "+w);
          t=-z;
          System.out.println("El valor de t es"+t);
          
          System.out.println("Operadores binarios");
          
        int a,b;
        a=7;
        b=4;
        int sum=a+b;
        int rest=a-b;
        int mult=a*b;
        int div=a/b;
        System.out.println("la suma es:  "+sum);
        System.out.println("la resta es:  "+rest);
        System.out.println("la multiplicacion es:  "+mult);
        System.out.println("la division es:  "+div);
        System.out.println("Comparaciones");
        int a11,b1,mayor;
        a11=7;
        b1=5;
        if (a11>b1)
        mayor = a11;
        else
         mayor = b1;
        
        
        
        
        System.out.println("Bitwise");
        int a111 = 2;
        int b111 = 3;
        int c = a111 & b111;
        int d = a111 | b111;
        int e = a111 ^ b111;

        System.out.println("La respuesta es: " + b111 + "   " + c + "     " + d + "      " + e);

        String sexo = "Mujer";
        int edad = 24;

        if ((sexo == "Mujer") && (edad == 25)) {
            System.out.println("Si eres mujer y tienes  25 anios ");
        } else {

            System.out.println("Alguna de las opciones no esta correcta  ");
        }

        if ((sexo == "Mujer") || (edad == 25)) {
            System.out.println("Si cumple alguna de las condiciones ");
        } else {

            System.out.println("Las dos opciones no coinciden ");
        }

        System.out.println("Uso de conversion");
        int entero = 900;
        String mensaje = String.valueOf(entero);

        System.out.println("La conversion de entero a string es" + mensaje);
        byte sw = 5;
        char sal;
        System.out.println("Promision aritmetica");

        short s11 = 9;
        int iii = 10;
        float f = 11.1f;
        double d6;

        d6 = 1234;
        if (-s11 * iii >= f / d6) {
            System.out.println(">=");
        } else {
            System.out.println("<");
        }

        System.out.println("uso de casting");
        double de;
        short s4;
        de = 1.255;

        s4 = (short) d;

        double dd = 1.55;
        int ii = (int) dd;
        System.out.println("la respuesta de casting es: " + ii);
        
        System.out.println("Uso de ramas probandoooooo");
        
        }
    
    
     
    
    
}
